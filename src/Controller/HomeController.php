<?php

namespace App\Controller;

use App\Entity\BibleBook;
use App\Entity\BibleKJ2000RawContent;
use App\Entity\BibleRVGRawContent;
use App\Helper\SearchHelper;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseController
{
    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    protected $searchHelper;


    public function __construct(PaginatorInterface $paginator, SearchHelper $searchHelper)
    {
        $this->paginator = $paginator;
        $this->searchHelper = $searchHelper;
    }

    /**
     * @Route("/{_locale}/", name="home_index", requirements={"_locale"="en|es"})
     */
    public function homeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $className = ($request->getLocale() == 'es')?
            'App:BibleRVGRawContent':'App:BibleKJ2000RawContent';

        $qb = $em->getRepository($className)
            ->createQueryBuilder('brc');

        $qb->where($qb->expr()->eq('brc.chapterNumber', 1))
            ->andWhere($qb->expr()->lt('brc.verseNumber', 4))
            ->innerJoin('brc.book', 'bb')
            ->innerJoin('brc.version', 'bv');

        $query = $qb->getQuery();

        $paginator  = $this->paginator;

        $bibleVerses = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 15)
        );

        return $this->render(
            'home/home.html.twig', [
                'browserForm' => $this->getBrowserForm([])->createView(),
                'searchForm' => $this->getSearchForm([])->createView(),
                'summaryForm' => $this->getSummaryForm([])->createView(),
                'bibleVerses' => $bibleVerses,
                'bibleBooks' => $this->loadSidebarBooksSection($request->getLocale())
            ]
        );
    }

    /**
     * @Route("/{_locale}/book/{id}", name = "home_book", defaults={"id"=1}, requirements={"_locale"="en|es"} )
     */
    public function bookAction(Request $request)
    {
        $form = $this->getBrowserForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $bookId = $form->get('book')->getData()->getId();
        } else {
            $bookId = $request->get('id');
        }

        $em = $this->getDoctrine()->getManager();

        $qbBibleBook = $em->getRepository(BibleBook::class)
            ->createQueryBuilder('bb');

        $qbBibleBook->where($qbBibleBook->expr()->eq('bb.id', ':id'))
            ->setParameter('id', $bookId);

        try {
            $bibleBook = $qbBibleBook->getQuery()
                ->setHint(
                    \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
                    'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
                )->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        if ($form->isSubmitted() && $form->isValid())
        {
            $chapter = $form->get('chapter')->getData();
            $verse = $form->get('verse')->getData();

            $page = $this->searchHelper->calculateVersePage(
                $bookId,
                $chapter,
                $verse,
                $request->getLocale()
            );
            return $this->redirect(
                $this->generateUrl(
                'home_book',
                ['id'=>$bookId]
            ).'?page='.$page.'#'.$bibleBook->getNameAbbreviation().$chapter.$verse);
        }



        $className = ($request->getLocale() == 'es')?
            BibleRVGRawContent::class:BibleKJ2000RawContent::class;

        $qbVerses = $em->getRepository($className)
            ->createQueryBuilder('brc');

        $qbVerses->where($qbVerses->expr()->eq('bb.id', ':id'))
            ->setParameter('id', $bookId)
            ->innerJoin('brc.book', 'bb')
            ->innerJoin('brc.version', 'bv');

        $queryVerses = $qbVerses->getQuery()->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        $paginator  = $this->paginator;

        $bibleVerses = $paginator->paginate(
            $queryVerses,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 15)
        );

        return $this->render(
            'home/book.html.twig', [
                'browserForm' => $this->getBrowserForm([])->createView(),
                'searchForm' => $this->getSearchForm([])->createView(),
                'summaryForm' => $this->getSummaryForm([])->createView(),
                'book' => $bibleBook,
                'bibleVerses' => $bibleVerses,
                'bibleBooks' => $this->loadSidebarBooksSection($request->getLocale())
            ]
        );
    }

    /**
     * @Route("/{_locale}/about_us", name="about_us", requirements={"_locale"="en|es"})
     */
    public function aboutUsAction(Request $request)
    {
        return $this->render(
            'home/about_us.html.twig', [
                'bibleBooks' => $this->loadSidebarBooksSection($request->getLocale())
            ]
        );
    }

    /**
     * @Route("/{_locale}/use_cases", name="use_cases", requirements={"_locale"="en|es"})
     */
    public function useCasesAction(Request $request)
    {
        return $this->render(
            'home/use_cases.html.twig', [
                'bibleBooks' => $this->loadSidebarBooksSection($request->getLocale())
            ]
        );
    }
}

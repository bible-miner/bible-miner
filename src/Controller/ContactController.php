<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends BaseController
{
    /**
     * @Route("/{_locale}/contact", name="contact")
     *
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(
            ContactType::class, null , [
                'action' => $this->generateUrl('contact')
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contactFormData = $form->getData();


            $message = (new \Swift_Message('Bible Miner: '. $contactFormData['subject']))
                ->setFrom($contactFormData['from'])
                ->setTo('webmaster@fluency-labs.com')
                ->setBody(
                    "From: {$contactFormData['from']}\n\n" .
                    $contactFormData['message'],
                    'text/plain'
                );
            try {
                $mailer->send($message);
                $this->addFlash('success', 'It sent!');
            } catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
            }

            return $this->redirectToRoute('contact');
        }

        return $this->render('contact/index.html.twig', [
            'contact_form' => $form->createView(),
            'bibleBooks' => $this->loadSidebarBooksSection($request->getLocale())
        ]);
    }
}

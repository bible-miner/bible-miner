<?php

namespace App\Controller;

use App\Entity\BibleBook;
use App\Entity\BibleKJ2000RawContent;
use App\Entity\BibleRVGRawContent;
use App\Helper\SearchHelper;
use Phpml\FeatureExtraction\TextRank\TextRankFacade;
use Phpml\FeatureExtraction\TextRank\Tool\StopWords\English;
use Phpml\FeatureExtraction\TextRank\Tool\StopWords\Spanish;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TextExtractionController extends BaseController
{
    /**
     * @var SearchHelper
     */
    protected $searchHelper;

    public function __construct(SearchHelper $searchHelper)
    {
        $this->searchHelper = $searchHelper;
    }


    /**
     * @Route("/{_locale}/extraction/summary/book",
     *     name="text_extraction_book_summary", requirements={"_locale"="en|es"}
     * )
     */
    public function bookSummaryAction(Request $request)
    {
        $form = $this->getSummaryForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $className = ($request->getLocale() == 'es') ?
                BibleRVGRawContent::class : BibleKJ2000RawContent::class;
            $queryBuilder = $this->getDoctrine()
                ->getRepository($className)
                ->createQueryBuilder('brc');

            $queryBuilder->select('brc.verseText')
                ->where($queryBuilder->expr()->eq('brc.book', $form->get('book')
                    ->getData()->getId())
                )
//                ->andWhere($queryBuilder->expr()->eq('brc.chapterNumber', $request->get('chapter')))
//                ->groupBy('brc.book, brc.chapterNumber, brc.verseText')
                ->orderBy('brc.id');
            $result = sprintf("%s",
                implode(
                    "",
                    array_map('current', $queryBuilder->getQuery()->getArrayResult()))
            );

            $textRank = new TextRankFacade();
            $stopWords = ($request->getLocale() == 'es') ?
                new Spanish() : new English();
            $textRank->setStopWords($stopWords);
            return $this->render(
                'text_extraction/summary.html.twig', [
                    'browserForm' => $this->getBrowserForm([])->createView(),
                    'searchForm' => $this->getSearchForm([])->createView(),
                    'bibleBooks' => $this->loadSidebarBooksSection($request->getLocale()),
                    'summaryForm' => $this->getSummaryForm([])->createView(),
                    'book' => $form->get('book')->getData(),
                    'summary' => $textRank->getHighlights($result),
                    'keyWords' => $textRank->getOnlyKeyWords($result),
                ]
            );
        } else {
            return $this->redirectToRoute('home_index');
        }
//            dump($textRank->getOnlyKeyWords($result));die;

    }

    /**
     * @Route("/{_locale}/extraction/summary/search",
     *     name="text_extraction_search_summary", requirements={"_locale"="en|es"}
     * )
     */
    public function searchSummaryAction(Request $request)
    {
        $rankedResults = $this->searchHelper
            ->getSuperRankedSearchQueryBuilder(array_keys($request->get('relevantTerms')))
            ->setMaxResults(100)->execute()->fetchAll();
        $textResult = implode (
            "",
            array_map(
                function ($record) {
                    return $record['verse_text'];
                }, $rankedResults
            )
        );

        $textRank = new TextRankFacade();
        $stopWords = ($request->getLocale() == 'es') ?
            new Spanish() : new English();
        $textRank->setStopWords($stopWords);
        return $this->render(
            'text_extraction/summary.html.twig', [
                'browserForm' => $this->getBrowserForm([])->createView(),
                'searchForm' => $this->getSearchForm(
                    [ 'searchTerms' => implode (" ", $request->get('relevantWords'))]
                )->createView(),
                'bibleBooks' => $this->loadSidebarBooksSection($request->getLocale()),
                'summaryForm' => $this->getSummaryForm([])->createView(),
                'pageTitle' => 'tools.search.title',
                'summary' => $textRank->getHighlights($textResult),
                'keyWords' => $textRank->getOnlyKeyWords($textResult),
            ]
        );
    }
}

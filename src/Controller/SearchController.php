<?php

namespace App\Controller;

use App\Helper\SearchHelper;
use Knp\Component\Pager\PaginatorInterface;
use Phpml\FeatureExtraction\TextRank\Tool\StopWords;
use Phpml\Tokenization\WordTokenizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Wamania\Snowball as Stemmer;

class SearchController extends BaseController
{
    /**
     * @var SearchHelper
     */
    protected $searchHelper;

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    protected $csrfTokenManager;

    protected $stemmer;

    public function __construct(SearchHelper $searchHelper, PaginatorInterface $paginator, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->searchHelper = $searchHelper;
        $this->paginator = $paginator;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @Route("/{_locale}/search/ranked", name="ranked_search", requirements={"_locale"="en|es"})
     */
    public function rankedSearchAction(Request $request)
    {
        if($request->getLocale() == 'es') {
            $this->stemmer = new Stemmer\Spanish();
            $stopWords = new StopWords\Spanish();
        } elseif ($request->getLocale() == 'en') {
            $this->stemmer = new Stemmer\English();
            $stopWords = new StopWords\English();
        }

        $form = $this->getSearchForm();
        $form->handleRequest($request);

        if(!$form->isSubmitted()) {
            $csrfToken = $this->csrfTokenManager->getToken('search_form');
            $submitData = [
                'searchTerms' => $request->query->get('searchTerms'),
                '_token' => $csrfToken->getValue()
            ];
            $form->submit($submitData);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $wordTokenizer = new WordTokenizer();
            $searchTerms = $wordTokenizer->tokenize($form->get('searchTerms')->getData());

            $stemmedTerms = [];
            foreach ($searchTerms as $searchTerm) {
                if(!$stopWords->exist(strtolower($searchTerm))) {
                    $stemmedTerms[$this->stemmer->stem($searchTerm)] = $searchTerm;
                }
            }

            $termScores = $this->searchHelper
                ->getStemmedTermsScore(array_keys($stemmedTerms));

            $relevantTerms = [];
            $relevantWords = [];
            foreach ($termScores as $stemmedTerm => $termScore) {
                $relevantTerms[$stemmedTerm] = [
                    'word' => strtolower($stemmedTerms[$stemmedTerm]),
                    'score' => round(10 - (float) $termScore['idf'],3)
                ];
                $relevantWords[] = strtolower($stemmedTerms[$stemmedTerm]);
            }

            if (isset($relevantTerms) && !$form->get('searchSummary')->getData()) {
                $rankedResults = $this->searchHelper
                    ->getSuperRankedSearchQueryBuilder(array_keys($relevantTerms));
            } elseif (isset($relevantTerms) && $form->get('searchSummary')->getData() == true) {
                return $this->redirectToRoute(
                    'text_extraction_search_summary',[
                        'relevantTerms' => $relevantTerms,
                        'relevantWords' => $relevantWords
                    ]
                );
            }

            $paginator = $this->paginator;

            $bibleVerses = $paginator->paginate(
                $rankedResults,
                $request->query->getInt('page', 1),
                $request->query->getInt('limit', 15)
            );
            return $this->render(
                'search/index.html.twig', [
                    'browserForm' => $this->getBrowserForm([])->createView(),
                    'searchForm' => $this->getSearchForm($form->getData())->createView(),
                    'summaryForm' => $this->getSummaryForm([])->createView(),
                    'relevantTerms' => $relevantTerms,
                    'bibleVerses' => $bibleVerses,
                    'bibleBooks' => $this->loadSidebarBooksSection($request->getLocale())
                ]
            );
        }
        return $this->redirectToRoute('home_index');
    }

    /**
     * @param Request $request
     * @Route("/{_locale}/search/chapters", name="chapters_search", requirements={"_locale"="en|es"})
     * @return JsonResponse
     */
    public function getBibleChaptersAction(Request $request)
    {
        $queryBuider = $this->searchHelper->getBookChaptersQueryBuilder(
            $request->get('bookId')
        );
        return new JsonResponse($queryBuider->execute()->fetchAll());
    }

    /**
     * @param Request $request
     * @Route("/{_locale}/search/verses", name="verses_search", requirements={"_locale"="en|es"})
     * @return JsonResponse
     */
    public function getBibleVersesAction(Request $request)
    {
        $queryBuider = $this->searchHelper->getChapterVersesQueryBuilder(
            $request->get('bookId'),
            $request->get('chapterNumber')
        );
        return new JsonResponse($queryBuider->execute()->fetchAll());
    }
}

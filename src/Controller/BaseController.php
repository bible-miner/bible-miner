<?php

namespace App\Controller;

use App\Entity\BibleBook;
use App\Form\BrowserType;
use App\Form\SearchType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FormType;

class BaseController extends AbstractController
{
    protected function loadSidebarBooksSection($locale) {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository(BibleBook::class)
            ->createQueryBuilder('bb');

        $firstColumn  = $qb->setFirstResult(0)->setMaxResults(39)->getQuery()
            ->setHint(
                \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
                'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
            )
            ->setHint(
                \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
                $locale
            )
            ->useResultCache(false)->getArrayResult();

        $secondColumn = $qb->setFirstResult(39)->orderBy('bb.id')->getQuery()
            ->setHint(
                \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
                'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
            )
            ->setHint(
                \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
                $locale
            )
            ->useResultCache(false)->getArrayResult();

        return [
            'firstColumn' => $firstColumn,
            'secondColumn' => $secondColumn,
        ];
    }

    protected function getSearchForm(array $data = [])
    {
        $form = $this->createForm(
            SearchType::class, $data , [
                'action' => $this->generateUrl('ranked_search'),
                'method' => 'get'
            ]
        );

        return $form;
    }

    protected function getBrowserForm(array $data = [])
    {
        $form = $this->createForm(
            BrowserType::class, $data , [
                'action' => $this->generateUrl('home_book'),
                'method' => 'get'
            ]
        );

        return $form;
    }

    protected function getSummaryForm(array $data = [])
    {
        return $this->get('form.factory')->createNamedBuilder(
            'summary_form', FormType::class, $data
        )
            ->add(
                'book', EntityType::class, [
                    'class' => BibleBook::class,
                    'label' => false,
                    'choice_label' => 'canonicalName',
                    'choice_value' => 'id',
                    'placeholder'   => 'tools.browser.book_placeholder',
                ]
            )->setAction($this->generateUrl('text_extraction_book_summary'))
            ->setMethod('get')->getForm();
    }

}
<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BibleRVGVerseStemmedVSMRepository")
 */
class BibleRVGVerseStemmedVSM
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var BibleRVGRawContent
     * @ORM\ManyToOne(targetEntity="BibleRVGRawContent")
     * @ORM\JoinColumn(name="verse_id", referencedColumnName="id")
     */
    protected $verse;

    /**
     * @var SpanishStemmedVocabulary
     * @ORM\ManyToOne(targetEntity="SpanishStemmedVocabulary")
     * @ORM\JoinColumn(name="word_id", referencedColumnName="id")
     */
    protected $word;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default"="0"})
     */
    protected $freqValue;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true, options={"default"="0"})
     */
    protected $tfIdfValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFreqValue(): ?int
    {
        return $this->freqValue;
    }

    public function setFreqValue(?int $freqValue): self
    {
        $this->freqValue = $freqValue;

        return $this;
    }

    public function getVerse(): ?BibleRVGRawContent
    {
        return $this->verse;
    }

    public function setVerse(?BibleRVGRawContent $verse): self
    {
        $this->verse = $verse;

        return $this;
    }

    public function getWord(): ?SpanishStemmedVocabulary
    {
        return $this->word;
    }

    public function setWord(?SpanishStemmedVocabulary $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getTfIdfValue(): ?float
    {
        return $this->tfIdfValue;
    }

    public function setTfIdfValue(?float $tfIdfValue): self
    {
        $this->tfIdfValue = $tfIdfValue;

        return $this;
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BibleRVGRawContentRepository")
 */
class BibleRVGRawContent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $chapterNumber;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $verseNumber;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $verseReference;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $verseText;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $verseTokens;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $stemmedVerseTokens;

    /**
     * @var BibleVersion
     * @ORM\ManyToOne(targetEntity="App\Entity\BibleVersion")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    protected $version;

    /**
     * @var BibleBook
     * @ORM\ManyToOne(targetEntity="App\Entity\BibleBook")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     */
    protected $book;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChapterNumber(): ?int
    {
        return $this->chapterNumber;
    }

    public function setChapterNumber(int $chapterNumber): self
    {
        $this->chapterNumber = $chapterNumber;

        return $this;
    }

    public function getVerseText(): ?string
    {
        return $this->verseText;
    }

    public function setVerseText(string $verseText): self
    {
        $this->verseText = $verseText;

        return $this;
    }

    public function getVerseNumber(): ?int
    {
        return $this->verseNumber;
    }

    public function setVerseNumber(int $verseNumber): self
    {
        $this->verseNumber = $verseNumber;

        return $this;
    }

    public function getVerseReference(): ?string
    {
        return $this->verseReference;
    }

    public function setVerseReference(string $verseReference): self
    {
        $this->verseReference = $verseReference;

        return $this;
    }

    public function getStemmedVerseTokens(): ?string
    {
        return $this->stemmedVerseTokens;
    }

    public function setStemmedVerseTokens(string $stemmedVerseTokens): self
    {
        $this->stemmedVerseTokens = $stemmedVerseTokens;

        return $this;
    }

    public function getBook(): ?BibleBook
    {
        return $this->book;
    }

    public function setBook(?BibleBook $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getVersion(): ?BibleVersion
    {
        return $this->version;
    }

    public function setVersion(?BibleVersion $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVerseTokens(): ?string
    {
        return $this->verseTokens;
    }

    public function setVerseTokens(string $verseTokens): self
    {
        $this->verseTokens = $verseTokens;

        return $this;
    }
}

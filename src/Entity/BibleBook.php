<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BibleBookRepository")
 */
class BibleBook implements Translatable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Gedmo\Translatable
     */
    protected $canonicalName;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $nameAbbreviation;

    /**
     * @var string
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getCanonicalName(): ?string
    {
        return $this->canonicalName;
    }

    public function setCanonicalName(string $canonicalName): self
    {
        $this->canonicalName = $canonicalName;

        return $this;
    }

    public function getNameAbbreviation(): ?string
    {
        return $this->nameAbbreviation;
    }

    public function setNameAbbreviation(string $nameAbbreviation): self
    {
        $this->nameAbbreviation = $nameAbbreviation;

        return $this;
    }


}

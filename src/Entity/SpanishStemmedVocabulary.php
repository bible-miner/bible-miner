<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\SpanishStemmedVocabularyRepository")
 * @UniqueEntity(fields={"word"})
 */
class SpanishStemmedVocabulary
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $stemmedWord;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default"="0"})
     */
    protected $documentFreq;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default"="0"})
     */
    protected $collectionFreq;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true, options={"default"="0"})
     */
    protected $idfValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStemmedWord(): ?string
    {
        return $this->stemmedWord;
    }

    public function setStemmedWord(string $stemmedWord): self
    {
        $this->stemmedWord = $stemmedWord;

        return $this;
    }

    public function getDocumentFreq(): ?int
    {
        return $this->documentFreq;
    }

    public function setDocumentFreq(?int $documentFreq): self
    {
        $this->documentFreq = $documentFreq;

        return $this;
    }

    public function getCollectionFreq(): ?int
    {
        return $this->collectionFreq;
    }

    public function setCollectionFreq(?int $collectionFreq): self
    {
        $this->collectionFreq = $collectionFreq;

        return $this;
    }

    public function getIdfValue(): ?float
    {
        return $this->idfValue;
    }

    public function setIdfValue(?float $idfValue): self
    {
        $this->idfValue = $idfValue;

        return $this;
    }
}

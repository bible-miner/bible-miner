<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Knp\Component\Pager\Event\ItemsEvent;
use Doctrine\DBAL\Query\QueryBuilder;

class KnpPaginationEventSubscriber implements EventSubscriberInterface
{
    public function items(ItemsEvent $event)
    {
        if ($event->target instanceof QueryBuilder) {
            /** @var QueryBuilder $target */
            $target = $event->target;

            $sql = $target->getSQL();

            $qb = clone $target;
            $qb->resetQueryParts()
                ->select('count(*) as cnt')
                ->from(sprintf('(%s)', $sql),'v_counter');
            $event->count = $qb->execute()
                ->fetchColumn(0);

            $event->items = [];
            if ($event->count) {
                $q = clone $target;
                $q->setFirstResult($event->getOffset())
                    ->setMaxResults($event->getLimit());
                $event->items = $q->execute()
                    ->fetchAll();
            }

            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            'knp_pager.items' => array('items', 10 /*make sure to transform before any further modifications*/)
        );
    }
}
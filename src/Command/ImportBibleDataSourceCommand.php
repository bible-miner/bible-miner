<?php

namespace App\Command;

use App\Entity\BibleBook;
use App\Entity\BibleKJ2000RawContent;
use App\Entity\BibleRVGRawContent;
use Doctrine\Bundle\DoctrineBundle\ConnectionFactory;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Phpml\Tokenization\WordTokenizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wamania\Snowball as Stemmer;
use Phpml\FeatureExtraction\TextRank\Tool\StopWords as StopWords;

class ImportBibleDataSourceCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'app:import:bible-data-source';

    /**
     * @var bool|string
     */
    protected $dataSourcePath;

    /**
     * @var ConnectionFactory
     */
    protected $connectionFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Container
     */
    protected $container;

    public function __construct($name = null, ConnectionFactory $connectionFactory,
                                EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->dataSourcePath = realpath('data/source');
        $this->em = $em;
        $this->connectionFactory = $connectionFactory;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Update bible miner database from data source.')
            ->addArgument(
                'datasource', InputArgument::OPTIONAL,
                'Data source Name [KJ2000, SpaRVG]', 'KJ2000'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $connection = $this->connectionFactory->createConnection(
            ['pdo' => new \PDO("sqlite:" . $this->dataSourcePath .
                DIRECTORY_SEPARATOR . $input->getArgument('datasource') . '.bib'
            )]
        );

        try {
            $stmt = $connection->executeQuery(
                "SELECT * FROM bible ORDER BY id ASC"
            );
            $bibleVerses = $stmt->fetchAll(\PDO::FETCH_OBJ);
        } catch (DBALException $e) {
            $io->error($e->getMessage());
        }

        if ($input->getArgument('datasource') == 'KJ2000') {
            $theBible = new BibleKJ2000RawContent();
            $bibleVersion = $this->em->getRepository('App:BibleVersion')
                ->findOneBy(['nameAbbreviation' => 'KJ2000']);
            $stemmer = new Stemmer\English();
            $stopWords = new StopWords\English();
        } elseif ($input->getArgument('datasource') == 'SpaRVG') {
            $theBible = new BibleRVGRawContent();
            $bibleVersion = $this->em->getRepository('App:BibleVersion')
                ->findOneBy(['nameAbbreviation' => 'SpaRVG']);
            $stemmer = new Stemmer\Spanish();
            $stopWords = new StopWords\Spanish();
        } else {
            $io->error('Datasource currently not suported.');
        }

        try {
            $io->note('Truncating existing data...');
            $this->container->get('bible.miner.helper.database')->truncateTable($theBible);
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }

        $io->text(sprintf('Processing %d verses...', count($bibleVerses)));
        $io->progressStart(count($bibleVerses));


        foreach ($bibleVerses as $bibleVerse) {

            if ($input->getArgument('datasource') == 'KJ2000') {
                $theBible = new BibleKJ2000RawContent();
            } elseif ($input->getArgument('datasource') == 'SpaRVG') {
                $theBible = new BibleRVGRawContent();
            } else {
                $io->error('Datasource currently not suported.');
            }

            $reference = preg_split('/\s|:/', $bibleVerse->ref);

            $bibleBook = $this->em->getRepository(BibleBook::class)
                ->findOneBy(['nameAbbreviation' => $reference[0]]);
            $theBible->setChapterNumber($reference[1]);
            $theBible->setVerseNumber($reference[2]);
            $theBible->setVerseReference($bibleVerse->ref);
            $theBible->setBook($bibleBook);

//            $theBible->setVerseText(str_replace(['<r>','</r>', '<i>', '</i>'], '', $bibleVerse->verse));
            $theBible->setVerseText($bibleVerse->verse);

            $wordTokenizer = new WordTokenizer();
            $verseWordsArray = array_map(
                'strtolower', $wordTokenizer->tokenize($bibleVerse->verse)
            );

            $stemmedVerse = array_filter(array_map(
                function($word) use ($stemmer, $stopWords) {
                    if(!$stopWords->exist($word)) {
                        return $stemmer->stem($word);
                    }
                    return false;
                }, $verseWordsArray
            ));

            $verseTokens = array_filter(array_map(
                function($word) use ($stopWords) {
                    if(!$stopWords->exist($word)) {
                        return $word;
                    }
                    return false;
                }, $verseWordsArray
            ));

            $theBible->setVerseTokens(implode(" ", $verseTokens));
            $theBible->setStemmedVerseTokens(implode(" ", $stemmedVerse));
            $theBible->setVersion($bibleVersion);

            $this->em->persist($theBible);
            $io->progressAdvance();
        }
        $io->progressFinish();
        $io->success('Text Processed Successfully.');
        $io->caution('Committing to database.');
        $this->em->flush();

        $io->success('Datasource imported successfully');
    }
}
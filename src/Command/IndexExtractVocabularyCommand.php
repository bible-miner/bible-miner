<?php

namespace App\Command;

use App\Entity\BibleKJ2000RawContent;
use App\Entity\BibleRVGRawContent;
use App\Entity\EnglishStemmedVocabulary;
use App\Entity\EnglishVocabulary;
use App\Entity\SpanishStemmedVocabulary;
use App\Entity\SpanishVocabulary;
use App\Helper\DatabaseHelper;
use Doctrine\ORM\EntityManagerInterface;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class IndexExtractVocabularyCommand extends Command
{
    protected static $defaultName = 'app:index:extract-vocabulary';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DatabaseHelper
     */
    protected $databaseHelper;

    public function __construct(?string $name = null, EntityManagerInterface $em, DatabaseHelper $databaseHelper)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->databaseHelper = $databaseHelper;
    }

    protected function configure()
    {
        $this
            ->setDescription('Extract Vocabulary')
            ->addArgument(
                'version', InputArgument::OPTIONAL,
                'Version shortname (\'SpaRVG\', \'KJ2000\',..).',
                'KJ2000'
            )
            ->addOption(
                'stemmed', 's', InputOption::VALUE_REQUIRED,
                'Use stemmed words.', 1
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getArgument('version') == 'KJ2000') {
            $bibleRepository = $this->em->getRepository(BibleKJ2000RawContent::class);
            $vocabularyEntity = new EnglishVocabulary();
            $stemmedVocabularyEntity = new EnglishStemmedVocabulary();
        } elseif ($input->getArgument('version') == 'SpaRVG') {
            $bibleRepository = $this->em->getRepository(BibleRVGRawContent::class);
            $vocabularyEntity = new SpanishVocabulary();
            $stemmedVocabularyEntity = new SpanishStemmedVocabulary();
        } else {
            $io->error('Version currently not suported.');
        }

        if($input->getOption('stemmed') == 1) {
            $vocabularyWordTokenizer = new TokenCountVectorizer(new WordTokenizer());

            $bibleVerseTokensQueryBuilder = $bibleRepository->createQueryBuilder('brc');

            $bibleVerseTokens = array_map(
                'current', $bibleVerseTokensQueryBuilder
                ->select('brc.stemmedVerseTokens')
                    ->getQuery()->getArrayResult()
            );

            $bibleVerseTokensQueryBuilder->resetDQLPart('select')->select('brc');
            $vocabularyWordTokenizer->fit($bibleVerseTokens);

            $bibleVocabulary = $vocabularyWordTokenizer->getVocabulary();

            $io->warning('Truncating stemmed vocabulary table.');
            $this->databaseHelper->truncateTable($stemmedVocabularyEntity);

            $io->note('Processing stemmed unique terms.');
            $io->progressStart(count($bibleVocabulary));
            foreach ($bibleVocabulary as $token) {
                $entity = clone $stemmedVocabularyEntity;
                $entity->setStemmedWord($token);
                $entity->setDocumentFreq(0);
                $entity->setCollectionFreq(0);
                $this->em->persist($entity);
                $io->progressAdvance();
            }
            $this->em->flush();
            $io->progressFinish();
        } else {
            $vocabularyWordTokenizer = new TokenCountVectorizer(new WordTokenizer());


            $bibleVerseTokens = $bibleRepository->createQueryBuilder('brc')
                ->select('brc.verseTokens')->getQuery()->getArrayResult();

            $bibleVerseTokens = array_map('current', $bibleVerseTokens);
            $vocabularyWordTokenizer->fit($bibleVerseTokens);

            $bibleVocabulary = $vocabularyWordTokenizer->getVocabulary();

            $io->warning('Truncating vocabulary table.');
            $this->databaseHelper->truncateTable($vocabularyEntity);

            $io->note('Processing unique terms.');
            $io->progressStart(count($bibleVocabulary));
            foreach ($bibleVocabulary as $token) {
                $entity = clone $vocabularyEntity;
                $entity->setWord($token);
                $entity->setDocumentFreq(0);
                $entity->setCollectionFreq(0);
                $this->em->persist($entity);
                $io->progressAdvance();
            }
            $this->em->flush();
            $io->progressFinish();
        }
    }
}

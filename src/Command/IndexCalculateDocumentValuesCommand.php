<?php

namespace App\Command;

use App\Entity\BibleKJ2000RawContent;
use App\Entity\BibleKJ2000VerseStemmedVSM;
use App\Entity\BibleKJ2000VerseVSM;
use App\Entity\BibleRVGRawContent;
use App\Entity\BibleRVGVerseStemmedVSM;
use App\Entity\BibleRVGVerseVSM;
use App\Entity\EnglishStemmedVocabulary;
use App\Entity\EnglishVocabulary;
use App\Entity\SpanishStemmedVocabulary;
use App\Entity\SpanishVocabulary;
use App\Helper\DatabaseHelper;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Phpml\Preprocessing\Normalizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class IndexCalculateDocumentValuesCommand extends Command
{
    protected static $defaultName = 'app:index:calculate:document-values';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DatabaseHelper
     */
    protected $databaseHelper;

    public function __construct(?string $name = null, EntityManagerInterface $em, DatabaseHelper $databaseHelper)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->databaseHelper = $databaseHelper;
    }

    protected function configure()
    {
        $this
            ->setDescription('Calculate Term Frequency, Document Frequency(DF) and Collection Frequency (CF)')
            ->addArgument(
                'version', InputArgument::OPTIONAL,
                'Version shortname (\'SpaRVG\', \'KJ2000\',..).',
                'KJ2000'
            )
            ->addOption(
                'stemmed', 's', InputOption::VALUE_REQUIRED,
                'Use stemmed words.', 1
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getArgument('version') == 'KJ2000') {
            $bibleRepository = $this->em->getRepository(BibleKJ2000RawContent::class);
            $vocabularyRepository = $this->em->getRepository(EnglishVocabulary::class);
            $stemmedVocabularyRepository = $this->em->getRepository(EnglishStemmedVocabulary::class);
            $verseVSMEntity = new BibleKJ2000VerseVSM();
            $stemmedVerseVSMEntity = new BibleKJ2000VerseStemmedVSM();
        } elseif ($input->getArgument('version') == 'SpaRVG') {
            $bibleRepository = $this->em->getRepository(BibleRVGRawContent::class);
            $vocabularyRepository = $this->em->getRepository(SpanishVocabulary::class);
            $stemmedVocabularyRepository = $this->em->getRepository(SpanishStemmedVocabulary::class);
            $verseVSMEntity = new BibleRVGVerseVSM();
            $stemmedVerseVSMEntity = new BibleRVGVerseStemmedVSM();
        } else {
            $io->error('Version currently not suported.');
            exit();
        }
        if($input->getOption('stemmed') == 1) {
            $verseFilteredTexts = array_map('current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.stemmedVerseTokens')->getQuery()->getArrayResult());
            $verseIds = array_map('current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.id')->getQuery()->getArrayResult());
            $vocabularyTokens = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.stemmedWord')->getQuery()->getArrayResult());
            $vocabularyTokenIdfValues = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.idfValue')->getQuery()->getArrayResult());
            $vocabularyTokenIds = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.id')->getQuery()->getArrayResult());
            $vocabularyTokenIdfValues = array_combine($vocabularyTokens, $vocabularyTokenIdfValues);
            $vocabularyTokens = array_combine($vocabularyTokens, $vocabularyTokenIds);


            $io->warning('Truncating stemmed vector space model table.');
            $this->databaseHelper->truncateTable($stemmedVerseVSMEntity);
            $classMetaData = $this->em->getClassMetadata(get_class($stemmedVerseVSMEntity));

            $io->progressStart(count($verseIds));
            $sql = sprintf(
                "INSERT INTO `%s` (`verse_id`, `word_id`, `freq_value`, `tf_idf_value`) VALUES\n",
                $classMetaData->getTableName()
            );

            $c = 0;
            foreach ($verseIds as $k => $verseId) {
                $tokens = array_filter(explode(' ', $verseFilteredTexts[$k]));
                $tokens = array_count_values($tokens);
                foreach ($tokens as $token => $tokenFreq) {
                    $c++;
                    $sql .= sprintf(
                        "(%d, %d, %d, " . $vocabularyTokenIdfValues[$token] * $tokenFreq . "),\n",
                        $verseId, $vocabularyTokens[$token], $tokenFreq
                    );
                }
                $io->progressAdvance();
            }
            $io->progressFinish();
            $sql = rtrim($sql, ",\n") . ";\n";

            try {
                $io->text(sprintf('Commiting %d records to database...', $c));
                $this->em->getConnection()->executeUpdate($sql);
                $io->success(sprintf('Commited %d records to database...', $c));
            } catch (DBALException $e) {
                $io->error($e->getMessage());
                exit();
            }
        } else {
            $verseFilteredTexts = array_map('current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.stemmedVerseTokens')->getQuery()->getArrayResult());
            $verseIds = array_map('current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.id')->getQuery()->getArrayResult());
            $vocabularyTokens = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.stemmedWord')->getQuery()->getArrayResult());
            $vocabularyTokenIdfValues = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.idfValue')->getQuery()->getArrayResult());
            $vocabularyTokenIds = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.id')->getQuery()->getArrayResult());
            $vocabularyTokenIdfValues = array_combine($vocabularyTokens, $vocabularyTokenIdfValues);
            $vocabularyTokens = array_combine($vocabularyTokens, $vocabularyTokenIds);


            $io->warning('Truncating vector space model table.');
            $this->databaseHelper->truncateTable($verseVSMEntity);
            $classMetaData = $this->em->getClassMetadata(get_class($verseVSMEntity));

            $io->progressStart(count($verseIds));
            $sql = sprintf(
                "INSERT INTO `%s` (`verse_id`, `word_id`, `freq_value`, `tf_idf_value`) VALUES\n",
                $classMetaData->getTableName()
            );

            $c = 0;
            foreach ($verseIds as $k => $verseId) {
                $tokens = array_filter(explode(' ', $verseFilteredTexts[$k]));
                $tokens = array_count_values($tokens);
                foreach ($tokens as $token => $tokenFreq) {
                    $c++;
                    $sql .= sprintf(
                        "(%d, %d, %d, " . $vocabularyTokenIdfValues[$token] * $tokenFreq . "),\n",
                        $verseId, $vocabularyTokens[$token], $tokenFreq
                    );
                }
                $io->progressAdvance();
            }
            $io->progressFinish();
            $sql = rtrim($sql, ",\n") . ";\n";

            try {
                $io->text(sprintf('Commiting %d records to database...', $c));
                $this->em->getConnection()->executeUpdate($sql);
                $io->success(sprintf('Commited %d records to database...', $c));
            } catch (DBALException $e) {
                $io->error($e->getMessage());
                exit();
            }
        }
    }
}

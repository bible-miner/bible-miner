<?php

namespace App\Command;

use App\Entity\BibleKJ2000RawContent;
use App\Entity\BibleRVGRawContent;
use App\Entity\EnglishStemmedVocabulary;
use App\Entity\EnglishVocabulary;
use App\Entity\SpanishStemmedVocabulary;
use App\Entity\SpanishVocabulary;
use App\Helper\DatabaseHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class IndexCalculateTermValuesCommand extends Command
{
    protected static $defaultName = 'app:index:calculate:term-values';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DatabaseHelper
     */
    protected $databaseHelper;

    public function __construct(?string $name = null, EntityManagerInterface $em, DatabaseHelper $databaseHelper)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->databaseHelper = $databaseHelper;
    }

    protected function configure()
    {
        $this
            ->setDescription('Calculate Document Frequency(DF) and Collection Frequency (CF)')
            ->addArgument(
                'version', InputArgument::OPTIONAL,
                'Version shortname (\'SpaRVG\', \'KJ2000\',..).',
                'KJ2000'
            )
            ->addOption(
                'stemmed', 's', InputOption::VALUE_REQUIRED,
                'Use stemmed words.', 1
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getArgument('version') == 'KJ2000') {
            $bibleRepository = $this->em->getRepository(BibleKJ2000RawContent::class);
            $vocabularyRepository = $this->em->getRepository(EnglishVocabulary::class);
            $stemmedVocabularyRepository = $this->em->getRepository(EnglishStemmedVocabulary::class);
        } elseif ($input->getArgument('version') == 'SpaRVG') {
            $bibleRepository = $this->em->getRepository(BibleRVGRawContent::class);
            $vocabularyRepository = $this->em->getRepository(SpanishVocabulary::class);
            $stemmedVocabularyRepository = $this->em->getRepository(SpanishStemmedVocabulary::class);
        } else {
            $io->error('Version currently not suported.');
        }

        if($input->getOption('stemmed') == 1) {
            $bibleVerseTokens = array_map(
                'current',
                $bibleRepository->createQueryBuilder('brc')->select('brc.stemmedVerseTokens')
                ->getQuery()->getArrayResult()
            );

            $bibleVerseTokensAll = array_filter(explode(' ', implode(' ', $bibleVerseTokens)));
            $cfValues = array_count_values($bibleVerseTokensAll);
            $totalVerses = count($bibleVerseTokens);

            $io->progressStart(count($cfValues));
            foreach ($cfValues as $word => $cfValue) {
                try {
                    $dfValue = $bibleRepository->createQueryBuilder('brc')
                        ->select('count(brc.id)')
                        ->where('brc.stemmedVerseTokens LIKE :wordM')
                        ->orWhere('brc.stemmedVerseTokens LIKE :wordB')
                        ->orWhere('brc.stemmedVerseTokens LIKE :wordE')
                        ->setParameter('wordM', '% ' . $word . ' %')
                        ->setParameter('wordB', $word . ' %')
                        ->setParameter('wordE', '% ' . $word)
                        ->getQuery()->getSingleScalarResult();
                } catch (NonUniqueResultException $e) {
                    $io->caution($e->getMessage());
                    exit($e->getCode());
                }
                /**
                 * @var EnglishStemmedVocabulary|SpanishStemmedVocabulary $vocabularyToken
                 */
                $vocabularyToken = $stemmedVocabularyRepository->findOneByStemmedWord($word);
                $vocabularyToken->setCollectionFreq($cfValue);
                $vocabularyToken->setDocumentFreq($dfValue);
                $vocabularyToken->setIdfValue(log($totalVerses/$dfValue,10));

                $this->em->persist($vocabularyToken);
                $io->progressAdvance();
            }
            $this->em->flush();
            $io->progressFinish();
        } else {
            $bibleVerseTokens = array_map(
                'current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.verseTokens')
                ->getQuery()->getArrayResult()
            );

            $bibleVerseTokensAll = array_filter(explode(' ', implode(' ', $bibleVerseTokens)));
            $cfValues = array_count_values($bibleVerseTokensAll);
            $totalVerses = count($bibleVerseTokens);

            $io->progressStart(count($cfValues));
            foreach ($cfValues as $word => $cfValue) {
                try {
                    $dfValue = $bibleRepository->createQueryBuilder('brc')
                        ->select('count(brc.id)')
                        ->where('brc.verseTokens LIKE :wordM')
                        ->orWhere('brc.verseTokens LIKE :wordB')
                        ->orWhere('brc.verseTokens LIKE :wordE')
                        ->setParameter('wordM', '% ' . $word . ' %')
                        ->setParameter('wordB', $word . ' %')
                        ->setParameter('wordE', '% ' . $word)
                        ->getQuery()->getSingleScalarResult();
                } catch (NonUniqueResultException $e) {
                    $io->caution($e->getMessage());
                    exit($e->getCode());
                }
                /**
                 * @var EnglishVocabulary|SpanishVocabulary $vocabularyToken
                 */
                $vocabularyToken = $vocabularyRepository->findOneByWord($word);
                $vocabularyToken->setCollectionFreq($cfValue);
                $vocabularyToken->setDocumentFreq($dfValue);
                $vocabularyToken->setIdfValue(log($totalVerses/$dfValue,10));

                $this->em->persist($vocabularyToken);
                $io->progressAdvance();
            }
            $this->em->flush();
            $io->progressFinish();
        }
    }
}

<?php

namespace App\Parser;


class RVGTextParser implements ParserInterface
{
    protected $sourceText;

    protected $bookNameAbbreviations = [];

    public function __construct($sourceText)
    {
        $this->sourceText = $sourceText;
    }

    public function parseSourceText()
    {
        // ^([Aa-Zz]+)\s+([0-9]+):([0-9]+)\s+(.*)
        $arrayText = [];
        preg_match_all(
            '/^(.{2,4})\s+([0-9]+)\:([0-9]+)\s+(.*)/ium',
            $this->sourceText,
            $arrayText
        );

        $this->bookNameAbbreviations = array_unique($arrayText[1]);

        return [
            'books' => $arrayText[1],
            'chapters' => $arrayText[2],
            'verses' => $arrayText[3],
            'text' => $arrayText[4]
        ];
    }

    public function getBooks() {
        return $this->bookNameAbbreviations;
    }
}
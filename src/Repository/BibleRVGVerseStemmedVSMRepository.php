<?php

namespace App\Repository;

use App\Entity\BibleRVGVerseStemmedVSM;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleRVGVerseStemmedVSM|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleRVGVerseStemmedVSM|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleRVGVerseStemmedVSM[]    findAll()
 * @method BibleRVGVerseStemmedVSM[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleRVGVerseStemmedVSMRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleKJ2000VerseStemmedVSM::class);
    }

//    /**
//     * @return BibleRVGVerseStemmedVSM[] Returns an array of BibleRVGVerseStemmedVSM objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleVerseStemmedVSM
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

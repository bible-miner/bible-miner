<?php

namespace App\Repository;

use App\Entity\BibleRVGVerseVSM;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleRVGVerseVSM|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleRVGVerseVSM|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleRVGVerseVSM[]    findAll()
 * @method BibleRVGVerseVSM[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleRVGVerseVSMRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleKJ2000VerseVSM::class);
    }

//    /**
//     * @return BibleRVGVerseVSM[] Returns an array of BibleRVGVerseVSM objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleVerseVSM
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

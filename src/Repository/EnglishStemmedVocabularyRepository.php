<?php

namespace App\Repository;

use App\Entity\EnglishStemmedVocabulary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EnglishStemmedVocabulary|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnglishStemmedVocabulary|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnglishStemmedVocabulary[]    findAll()
 * @method EnglishStemmedVocabulary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnglishStemmedVocabularyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EnglishStemmedVocabulary::class);
    }

//    /**
//     * @return EnglishStemmedVocabulary[] Returns an array of EnglishStemmedVocabulary objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleStemmedVocabulary
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

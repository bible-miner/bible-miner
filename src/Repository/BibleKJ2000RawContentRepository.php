<?php

namespace App\Repository;

use App\Entity\BibleKJ2000RawContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleKJ2000RawContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleKJ2000RawContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleKJ2000RawContent[]    findAll()
 * @method BibleKJ2000RawContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleKJ2000RawContentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleKJ2000RawContent::class);
    }

    // /**
    //  * @return BibleKJ2000RawContent[] Returns an array of BibleKJ2000RawContent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KJ2000RawContent
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

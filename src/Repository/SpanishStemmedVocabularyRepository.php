<?php

namespace App\Repository;

use App\Entity\SpanishStemmedVocabulary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SpanishStemmedVocabulary|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpanishStemmedVocabulary|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpanishStemmedVocabulary[]    findAll()
 * @method SpanishStemmedVocabulary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpanishStemmedVocabularyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SpanishStemmedVocabulary::class);
    }

//    /**
//     * @return SpanishStemmedVocabulary[] Returns an array of SpanishStemmedVocabulary objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleStemmedVocabulary
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\SpanishVocabulary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SpanishVocabulary|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpanishVocabulary|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpanishVocabulary[]    findAll()
 * @method SpanishVocabulary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpanishVocabularyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SpanishVocabulary::class);
    }

//    /**
//     * @return SpanishVocabulary[] Returns an array of SpanishVocabulary objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleVocabulary
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

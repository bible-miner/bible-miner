<?php

namespace App\Repository;

use App\Entity\BibleRVGRawContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleRVGRawContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleRVGRawContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleRVGRawContent[]    findAll()
 * @method BibleRVGRawContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleRVGRawContentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleRVGRawContent::class);
    }

//    /**
//     * @return BibleRVGRawContent[] Returns an array of BibleRVGRawContent objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleRawContent
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

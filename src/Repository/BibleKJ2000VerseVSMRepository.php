<?php

namespace App\Repository;

use App\Entity\BibleKJ2000VerseVSM;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleKJ2000VerseVSM|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleKJ2000VerseVSM|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleKJ2000VerseVSM[]    findAll()
 * @method BibleKJ2000VerseVSM[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleKJ2000VerseVSMRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleKJ2000VerseVSM::class);
    }

//    /**
//     * @return BibleKJ2000VerseVSM[] Returns an array of BibleKJ2000VerseVSM objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleVerseVSM
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

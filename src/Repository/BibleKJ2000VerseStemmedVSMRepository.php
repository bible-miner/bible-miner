<?php

namespace App\Repository;

use App\Entity\BibleKJ2000VerseStemmedVSM;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleKJ2000VerseStemmedVSM|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleKJ2000VerseStemmedVSM|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleKJ2000VerseStemmedVSM[]    findAll()
 * @method BibleKJ2000VerseStemmedVSM[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleKJ2000VerseStemmedVSMRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleKJ2000VerseStemmedVSM::class);
    }

//    /**
//     * @return BibleKJ2000VerseStemmedVSM[] Returns an array of BibleKJ2000VerseStemmedVSM objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleVerseStemmedVSM
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

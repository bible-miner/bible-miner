<?php

namespace App\Repository;

use App\Entity\EnglishVocabulary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EnglishVocabulary|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnglishVocabulary|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnglishVocabulary[]    findAll()
 * @method EnglishVocabulary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnglishVocabularyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EnglishVocabulary::class);
    }

//    /**
//     * @return EnglishVocabulary[] Returns an array of EnglishVocabulary objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleVocabulary
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

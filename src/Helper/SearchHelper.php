<?php

namespace App\Helper;

use App\Entity\BibleKJ2000VerseStemmedVSM;
use App\Entity\BibleRVGVerseStemmedVSM;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class SearchHelper
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    public function getStemmedTermsScore($stemmedSearchTerms)
    {
        if ($this->requestStack->getCurrentRequest()->getLocale() == 'es'){
            $language = "spanish";
        } elseif ($this->requestStack->getCurrentRequest()->getLocale()) {
            $language = "english";
        }

        $connection = $this->entityManager->getConnection();
        $queryBuilder = $connection->createQueryBuilder();
        try {
            $queryBuilder->select(
                'sv.stemmed_word, sv.collection_freq AS cf',
                'sv.document_freq AS df, sv.idf_value AS idf'
            )
                ->from("{$language}_stemmed_vocabulary", 'sv')
                ->where($queryBuilder->expr()->in('sv.stemmed_word', ':words'))
                ->setParameter('words', $stemmedSearchTerms, Connection::PARAM_STR_ARRAY)
                ->orderBy('idf', 'ASC');

            $result = $queryBuilder->execute()->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE);

            return $result;

        } catch (DBALException $e) {

        }
    }

    public function verseStemmedRawSearch($stemmedSearchTerms,  $limit = null, $offset = 0)
    {
        if ($this->requestStack->getCurrentRequest()->getLocale() == 'es'){
            $language = "spanish";
            $version = 'rvg';
        } elseif ($this->requestStack->getCurrentRequest()->getLocale() == 'en') {
            $language = "english";
            $version = 'kj2000';
        }

        $connection = $this->entityManager->getConnection();
        $queryBuilder = $connection->createQueryBuilder();

        try {
            $queryBuilder->select('v1.ref, v1.word, brc.verse_text, v1.tf, v1.idf, v1.tfxidf')
                ->from("v_stemmed_{$language}_tfxidf_scoring", 'v1')
                ->innerJoin(
                    'v1', "bible_{$version}raw_content", 'brc',
                    'v1.verse_id = brc.id'
                )
                ->setFirstResult($offset)
                ->where($queryBuilder->expr()->in('v1.word', ':terms'))
                ->setParameter('terms', $stemmedSearchTerms, Connection::PARAM_STR_ARRAY)
                ->orderBy('v1.verse_id', 'ASC')
                ->addOrderBy('v1.word', 'ASC');
            if(!is_null($limit)) {
                $queryBuilder->setMaxResults($limit);
            }

            $result = $queryBuilder->execute()->fetchAll();

            return $result;

        } catch (DBALException $e) {

        }
    }

    public function verseStemmedSuperRankedSearch($stemmedSearchTerms,  $limit = null, $offset = 0)
    {
        $mainQb = $this->getSuperRankedSearchQueryBuilder($stemmedSearchTerms);

        if(!is_null($limit)) {
            $mainQb->setMaxResults($limit)->setFirstResult($offset);
        }

        return $mainQb->execute()->fetchAll();
    }

    public function getSuperRankedSearchQueryBuilder($stemmedSearchTerms) {
        if ($this->requestStack->getCurrentRequest()->getLocale() == 'es'){
            $language = "spanish";
            $version = 'rvg';
        } elseif ($this->requestStack->getCurrentRequest()->getLocale() == 'en') {
            $language = "english";
            $version = 'kj2000';
        }
        $connection = $this->entityManager->getConnection();
        $subQb = $connection->createQueryBuilder();
        $mainQb = $connection->createQueryBuilder();

        $subQb->select(
            "bvsv2.verse_id,
                 GROUP_CONCAT(DISTINCT sv.stemmed_word SEPARATOR ',') AS words,
                 COUNT(*) AS CountOf, SUM(sv.idf_value) AS SumOf"
        )
        ->from("bible_{$version}verse_stemmed_vsm", 'bvsv2')
        ->innerJoin(
            'bvsv2', "{$language}_stemmed_vocabulary", 'sv',
            'sv.id = bvsv2.word_id'
        )
        ->where(
            $subQb->expr()->in(
                'sv.stemmed_word', ':words'
            )
        )
        ->groupBy('bvsv2.verse_id')
        ->having('COUNT(*) > 1');

        $mainQb->select(
            "brc.id as verse_id, brc.verse_reference AS ref, 
            brc.verse_text AS verse_text, v2.words AS words, 
            bb.canonical_name AS bibleBookCanonicalName,
            v2.CountOf AS matches, v2.SumOf AS score"
        )
        ->from("bible_{$version}verse_stemmed_vsm", 'bvsv')
        ->innerJoin(
            'bvsv', "bible_{$version}raw_content",
            'brc', 'brc.id = bvsv.verse_id'
        )
        ->innerJoin(
            'brc', "bible_book",
            'bb', 'bb.id = brc.book_id'
        )
        ->innerJoin(
            'bvsv', sprintf('(%s)', $subQb->getSQL()), 'v2',
            'v2.verse_id = bvsv.verse_id'
        )
        ->setParameter('words', $stemmedSearchTerms, Connection::PARAM_STR_ARRAY)
        ->groupBy('brc.id')
        ->orderBy('matches', 'DESC')
        ->addOrderBy('score', 'DESC');

        return $mainQb;
    }

    public function getBookChaptersQueryBuilder($bookId)
    {
        if ($this->requestStack->getCurrentRequest()->getLocale() == 'es'){
            $language = "spanish";
            $version = 'rvg';
        } elseif ($this->requestStack->getCurrentRequest()->getLocale() == 'en') {
            $language = "english";
            $version = 'kj2000';
        }
        $connection = $this->entityManager->getConnection();

        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder->select('DISTINCT brc.chapter_number')
            ->from("bible_{$version}raw_content", 'brc')
            ->where($queryBuilder->expr()->eq('brc.book_id', $bookId));
        return $queryBuilder;
    }

    public function getChapterVersesQueryBuilder($bookId, $chapterNumber)
    {
        if ($this->requestStack->getCurrentRequest()->getLocale() == 'es'){
            $version = 'rvg';
        } elseif ($this->requestStack->getCurrentRequest()->getLocale() == 'en') {
            $version = 'kj2000';
        }
        $connection = $this->entityManager->getConnection();

        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder->select('DISTINCT brc.verse_number')
            ->from("bible_{$version}raw_content", 'brc')
            ->where($queryBuilder->expr()->eq('brc.chapter_number', $chapterNumber))
            ->andWhere($queryBuilder->expr()->eq('brc.book_id', $bookId));
        return $queryBuilder;

    }

    public function calculateVersePage($bookId, $chapter, $verse)
    {
        if ($this->requestStack->getCurrentRequest()->getLocale() == 'es'){
            $version = 'rvg';
        } elseif ($this->requestStack->getCurrentRequest()->getLocale() == 'en') {
            $version = 'kj2000';
        }

        $connection = $this->entityManager->getConnection();

        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder->select('CONCAT(brc.chapter_number, ":", brc.verse_number)')
            ->from("bible_{$version}raw_content", 'brc')
            ->where($queryBuilder->expr()->eq('brc.book_id', $bookId));
        $result = $queryBuilder->execute()->fetchAll(\PDO::FETCH_COLUMN);
        $page = (int) ceil((array_search("{$chapter}:{$verse}", $result) + 1)/15);

        return $page;
    }
}
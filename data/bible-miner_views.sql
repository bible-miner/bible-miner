-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 22, 2019 at 01:51 AM
-- Server version: 5.7.25-0ubuntu0.16.04.2-log
-- PHP Version: 7.2.15-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bible-miner`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stemmed_english_cf_df_scoring`
-- (See below for the actual view)
--
CREATE TABLE `v_stemmed_english_cf_df_scoring` (
`id` int(11)
,`stemmed_word` varchar(255)
,`df` bigint(21)
,`cf` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stemmed_english_idf_scoring`
-- (See below for the actual view)
--
CREATE TABLE `v_stemmed_english_idf_scoring` (
`id` int(11)
,`stemmed_word` varchar(255)
,`idf` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stemmed_english_tfxidf_scoring`
-- (See below for the actual view)
--
CREATE TABLE `v_stemmed_english_tfxidf_scoring` (
`verse_id` int(11)
,`ref` varchar(255)
,`word` varchar(255)
,`tf` int(11)
,`idf` double
,`tfxidf` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stemmed_spanish_cf_df_scoring`
-- (See below for the actual view)
--
CREATE TABLE `v_stemmed_spanish_cf_df_scoring` (
`id` int(11)
,`stemmed_word` varchar(255)
,`df` bigint(21)
,`cf` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stemmed_spanish_idf_scoring`
-- (See below for the actual view)
--
CREATE TABLE `v_stemmed_spanish_idf_scoring` (
`id` int(11)
,`stemmed_word` varchar(255)
,`idf` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_stemmed_spanish_tfxidf_scoring`
-- (See below for the actual view)
--
CREATE TABLE `v_stemmed_spanish_tfxidf_scoring` (
`verse_id` int(11)
,`ref` varchar(255)
,`word` varchar(255)
,`tf` int(11)
,`idf` double
,`tfxidf` double
);

-- --------------------------------------------------------

--
-- Structure for view `v_stemmed_english_cf_df_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_english_cf_df_scoring`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`bible-miner`@`localhost` SQL SECURITY DEFINER VIEW `v_stemmed_english_cf_df_scoring`  AS  select `bsv`.`id` AS `id`,`bsv`.`stemmed_word` AS `stemmed_word`,count(`bvsvsm`.`word_id`) AS `df`,sum(`bvsvsm`.`freq_value`) AS `cf` from (`english_stemmed_vocabulary` `bsv` join `bible_kj2000verse_stemmed_vsm` `bvsvsm` on((`bsv`.`id` = `bvsvsm`.`word_id`))) group by `bsv`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stemmed_english_idf_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_english_idf_scoring`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`bible-miner`@`localhost` SQL SECURITY DEFINER VIEW `v_stemmed_english_idf_scoring`  AS  select `bsv`.`id` AS `id`,`bsv`.`stemmed_word` AS `stemmed_word`,ifnull(log(10,((select count(distinct `bvsvsm1`.`verse_id`) from `bible_kj2000verse_stemmed_vsm` `bvsvsm1`) / (select count(`bvsvsm2`.`word_id`) AS `df` from (`english_stemmed_vocabulary` `bsv2` join `bible_kj2000verse_stemmed_vsm` `bvsvsm2` on((`bsv2`.`id` = `bvsvsm2`.`word_id`))) where (`bsv2`.`id` = `bsv`.`id`)))),0) AS `idf` from `english_stemmed_vocabulary` `bsv` order by `bsv`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stemmed_english_tfxidf_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_english_tfxidf_scoring`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`bible-miner`@`localhost` SQL SECURITY DEFINER VIEW `v_stemmed_english_tfxidf_scoring`  AS  select `brc`.`id` AS `verse_id`,`brc`.`verse_reference` AS `ref`,`vsis`.`stemmed_word` AS `word`,`bvsv`.`freq_value` AS `tf`,`vsis`.`idf` AS `idf`,(`bvsv`.`freq_value` * `vsis`.`idf`) AS `tfxidf` from ((`bible_kj2000verse_stemmed_vsm` `bvsv` join `bible_kj2000raw_content` `brc` on((`bvsv`.`verse_id` = `brc`.`id`))) join `v_stemmed_english_idf_scoring` `vsis` on((`bvsv`.`word_id` = `vsis`.`id`))) order by `brc`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stemmed_spanish_cf_df_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_spanish_cf_df_scoring`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`bible-miner`@`localhost` SQL SECURITY DEFINER VIEW `v_stemmed_spanish_cf_df_scoring`  AS  select `bsv`.`id` AS `id`,`bsv`.`stemmed_word` AS `stemmed_word`,count(`bvsvsm`.`word_id`) AS `df`,sum(`bvsvsm`.`freq_value`) AS `cf` from (`spanish_stemmed_vocabulary` `bsv` join `bible_rvgverse_stemmed_vsm` `bvsvsm` on((`bsv`.`id` = `bvsvsm`.`word_id`))) group by `bsv`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stemmed_spanish_idf_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_spanish_idf_scoring`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`bible-miner`@`localhost` SQL SECURITY DEFINER VIEW `v_stemmed_spanish_idf_scoring`  AS  select `bsv`.`id` AS `id`,`bsv`.`stemmed_word` AS `stemmed_word`,ifnull(log(10,((select count(distinct `bvsvsm1`.`verse_id`) from `bible_rvgverse_stemmed_vsm` `bvsvsm1`) / (select count(`bvsvsm2`.`word_id`) AS `df` from (`spanish_stemmed_vocabulary` `bsv2` join `bible_rvgverse_stemmed_vsm` `bvsvsm2` on((`bsv2`.`id` = `bvsvsm2`.`word_id`))) where (`bsv2`.`id` = `bsv`.`id`)))),0) AS `idf` from `spanish_stemmed_vocabulary` `bsv` order by `bsv`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_stemmed_spanish_tfxidf_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_spanish_tfxidf_scoring`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`bible-miner`@`localhost` SQL SECURITY DEFINER VIEW `v_stemmed_spanish_tfxidf_scoring`  AS  select `brc`.`id` AS `verse_id`,`brc`.`verse_reference` AS `ref`,`vsis`.`stemmed_word` AS `word`,`bvsv`.`freq_value` AS `tf`,`vsis`.`idf` AS `idf`,(`bvsv`.`freq_value` * `vsis`.`idf`) AS `tfxidf` from ((`bible_rvgverse_stemmed_vsm` `bvsv` join `bible_rvgraw_content` `brc` on((`bvsv`.`verse_id` = `brc`.`id`))) join `v_stemmed_spanish_idf_scoring` `vsis` on((`bvsv`.`word_id` = `vsis`.`id`))) order by `brc`.`id` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- Get cf & df *AIIR
SELECT
  bsv.id,
  bsv.stemmed_word,
  COUNT(bvsfvsm.word_id)  AS df,
  SUM(bvsfvsm.freq_value) AS cf
FROM `bible_stemmed_vocabulary` bsv
  INNER JOIN bible_verse_stemmed_fvsm bvsfvsm
    ON bsv.id = bvsfvsm.word_id
GROUP BY bsv.id
ORDER BY cf DESC;
-- Term df(dios)
SELECT COUNT(brc.verse_reference)
FROM bible_raw_content brc
  INNER JOIN bible_verse_stemmed_fvsm bvsfvsm
    ON brc.id = bvsfvsm.verse_id
  INNER JOIN bible_stemmed_vocabulary bsv
    ON bvsfvsm.word_id = bsv.id
WHERE bsv.stemmed_word = 'dios';

-- Detect non consecutive numbers (i.e. Verse 4939 has 0 score)
SELECT A.verse_id + 1
FROM bible_verse_stemmed_fvsm AS A
WHERE NOT EXISTS(
    SELECT B.verse_id
    FROM bible_verse_stemmed_fvsm AS B
    WHERE A.verse_id + 1 = B.verse_id)
GROUP BY A.verse_id;

-- Get N as ALREADY INDEXED DOCUMENTS
SELECT COUNT(DISTINCT `verse_id`)
FROM `bible_verse_stemmed_fvsm`;

-- Calculate IDF
SELECT
  bsv.id,
  bsv.stemmed_word,
  IFNULL(LOG(10,
             (SELECT COUNT(DISTINCT bvsfvsm1.verse_id)
              FROM `bible_verse_stemmed_fvsm` bvsfvsm1) /
             (SELECT COUNT(bvsfvsm2.word_id) AS df
              FROM `bible_stemmed_vocabulary` bsv2
                INNER JOIN bible_verse_stemmed_fvsm bvsfvsm2
                  ON bsv2.id = bvsfvsm2.word_id
              WHERE bsv2.id = bsv.id
             )
         ), 0) AS idf
FROM bible_stemmed_vocabulary bsv
ORDER BY bsv.stemmed_word ASC;

SELECT
  bv.id,
  bv.word,
  IFNULL(LOG(10,
             (SELECT COUNT(DISTINCT bvfvsm1.verse_id)
              FROM `bible_verse_fvsm` bvfvsm1) /
             (SELECT COUNT(bvfvsm2.word_id) AS df
              FROM `bible_vocabulary` bv2
                INNER JOIN bible_verse_fvsm bvfvsm2
                  ON bv2.id = bvfvsm2.word_id
              WHERE bv2.id = bv.id
             )
         ), 0) AS idf
FROM bible_vocabulary bv
ORDER BY bv.word ASC;

-- Calculate TF-IDF

SELECT
  bvs.id                        AS word_id,
  brc.verse_reference,
  v.stemmed_word                AS word,
  bvs.freq_value                AS tf,
  IFNULL(LOG(10,
             (SELECT COUNT(DISTINCT bvsvsm1.verse_id)
              FROM `bible_verse_stemmed_vsm` bvsvsm1) /
             (SELECT COUNT(bvsvsm2.word_id) AS df
              FROM `bible_stemmed_vocabulary` bsv2
                INNER JOIN bible_verse_stemmed_vsm bvsvsm2
                  ON bsv2.id = bvsvsm2.word_id
              WHERE bsv2.id = v.id
             )
         ), 0)                  AS idf,
  bvs.freq_value * IFNULL(LOG(10,
                              (SELECT COUNT(DISTINCT bvsvsm1.verse_id)
                               FROM `bible_verse_stemmed_vsm` bvsvsm1) /
                              (SELECT COUNT(bvsvsm2.word_id) AS df
                               FROM `bible_stemmed_vocabulary` bsv2
                                 INNER JOIN bible_verse_stemmed_vsm bvsvsm2
                                   ON bsv2.id = bvsvsm2.word_id
                               WHERE bsv2.id = v.id
                              )
                          ), 0) AS tfidf
FROM bible_raw_content brc
  INNER JOIN bible_verse_stemmed_vsm bvs ON brc.id = bvs.verse_id
  INNER JOIN bible_stemmed_vocabulary v ON bvs.word_id = v.id
ORDER BY v.stemmed_word ASC;

SELECT
  brc.id                       AS verse_id,
  brc.verse_reference          AS ref,
  sv.id                        AS word_id,
  vsis.stemmed_word            AS word,
  bvsv.freq_value              AS tf,
  (bvsv.freq_value * vsis.idf) AS tfxidf
FROM `bible_verse_stemmed_vsm` bvsv
  INNER JOIN bible_stemmed_vocabulary sv ON bvsv.word_id = sv.id
  INNER JOIN bible_raw_content brc ON bvsv.verse_id = brc.id
  INNER JOIN v_stemmed_idf_scoring vsis ON bvsv.word_id = vsis.id
ORDER BY brc.id ASC;

-- v_stemmed_cf_df_scoring
CREATE ALGORITHM = TEMPTABLE
  DEFINER =`bible-miner`@`localhost`
  SQL SECURITY DEFINER VIEW `v_stemmed_cf_df_scoring`  AS
  SELECT
    `bsv`.`id`                 AS `id`,
    `bsv`.`stemmed_word`       AS `stemmed_word`,
    count(`bvsvsm`.`word_id`)  AS `df`,
    sum(`bvsvsm`.`freq_value`) AS `cf`
  FROM (`bible_stemmed_vocabulary` `bsv`
    JOIN `bible_verse_stemmed_vsm` `bvsvsm` ON ((`bsv`.`id` = `bvsvsm`.`word_id`)))
  GROUP BY `bsv`.`id`;

-- v_stemmed_idf_scoring
CREATE ALGORITHM = TEMPTABLE
  DEFINER =`bible-miner`@`localhost`
  SQL SECURITY DEFINER VIEW `v_stemmed_idf_scoring`  AS
  SELECT
    `bsv`.`id`                                               AS `id`,
    `bsv`.`stemmed_word`                                     AS `stemmed_word`,
    ifnull(log(10, ((SELECT count(DISTINCT `bvsvsm1`.`verse_id`)
                     FROM `bible_verse_stemmed_vsm` `bvsvsm1`) /
                    (SELECT count(`bvsvsm2`.`word_id`) AS `df`
                     FROM (`bible_stemmed_vocabulary` `bsv2`
                       JOIN `bible_verse_stemmed_vsm` `bvsvsm2` ON ((`bsv2`.`id` = `bvsvsm2`.`word_id`)))
                     WHERE (`bsv2`.`id` = `bsv`.`id`)))), 0) AS `idf`
  FROM `bible_stemmed_vocabulary` `bsv`
  ORDER BY `bsv`.`id`;

-- v_stemmed_tfxidf_scoring
CREATE ALGORITHM = TEMPTABLE
  DEFINER =`bible-miner`@`localhost`
  SQL SECURITY DEFINER VIEW `v_stemmed_tfxidf_scoring`  AS
  SELECT
    `brc`.`id`                           AS `verse_id`,
    `brc`.`verse_reference`              AS `ref`,
    `vsis`.`stemmed_word`                AS `word`,
    `bvsv`.`freq_value`                  AS `tf`,
    (`bvsv`.`freq_value` * `vsis`.`idf`) AS `tfxidf`
  FROM ((`bible_verse_stemmed_vsm` `bvsv`
    JOIN `bible_raw_content` `brc` ON ((`bvsv`.`verse_id` = `brc`.`id`)))
    JOIN `v_stemmed_idf_scoring` `vsis` ON ((`bvsv`.`word_id` = `vsis`.`id`)))
  ORDER BY `brc`.`id`;

-- ranked search verses having more than one query terms
SELECT
  v1.verse_id,
  v1.ref,
  v1.word,
  v1.tf,
  v1.idf,
  v1.tfxidf
FROM v_stemmed_tfxidf_scoring v1
  INNER JOIN (SELECT
                verse_id,
                COUNT(*) AS CountOf
              FROM v_stemmed_tfxidf_scoring
              WHERE word IN ('dios', 'castig', 'pec')
              GROUP BY verse_id
              HAVING COUNT(*) > 1
             ) v2 ON v1.verse_id = v2.verse_id
WHERE v1.word IN ('dios', 'castig', 'pec')
ORDER BY v1.ref ASC, v1.idf ASC, v1.tfxidf DESC;

-- misc
SELECT *
FROM `v_stemmed_idf_scoring`
WHERE stemmed_word
      IN ('dios', 'pec', 'espiritu', 'fe', 'amor', 'perdon', 'etern', 'castig', 'infiern', 'paraiso')

SELECT
  v1.verse_id,
  v1.ref,
  v1.word,
  v1.tf,
  v1.idf,
  v1.tfxidf
FROM v_stemmed_tfxidf_scoring v1
WHERE v1.word IN ('dios', 'pec', 'espiritu', 'fe', 'amor', 'perdon', 'etern', 'castig', 'infiern', 'paraiso')
ORDER BY v1.tfxidf DESC, v1.ref ASC, v1.idf DESC;

SELECT
  ref,
  verse_text,
  GROUP_CONCAT(DISTINCT word SEPARATOR ',') AS words,
  COUNT(ref)                                AS matches,
  SUM(tfxidf)                               AS score
FROM (
       SELECT
         v1.verse_id,
         brc.verse_text,
         v1.ref,
         v1.word,
         v1.tf,
         v1.idf,
         v1.tfxidf
       FROM v_stemmed_tfxidf_scoring v1
         INNER JOIN (
                      SELECT
                        verse_id,
                        COUNT(*) AS CountOf
                      FROM v_stemmed_tfxidf_scoring
                      WHERE word IN
                            ('dios', 'pec', 'espiritu', 'fe', 'amor', 'perdon', 'etern', 'castig', 'infiern', 'paraiso')
                      GROUP BY verse_id
                      HAVING COUNT(*) > 1
                    ) v2 ON v1.verse_id = v2.verse_id
         INNER JOIN bible_raw_content brc ON v1.verse_id = brc.id
       WHERE v1.word IN ('dios', 'pec', 'espiritu', 'fe', 'amor', 'perdon', 'etern', 'castig', 'infiern', 'paraiso')
       ORDER BY v1.ref ASC, v1.tfxidf DESC, v1.idf DESC) v

GROUP BY ref, verse_text
ORDER BY matches DESC, score DESC;

--
-- Structure for view `v_stemmed_cf_df_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_cf_df_scoring`;

CREATE ALGORITHM = TEMPTABLE
  DEFINER =`bible-miner`@`localhost`
  SQL SECURITY DEFINER VIEW
  `v_stemmed_cf_df_scoring`  AS
  SELECT
    `bsv`.`id`                 AS `id`,
    `bsv`.`stemmed_word`       AS `stemmed_word`,
    count(`bvsvsm`.`word_id`)  AS `df`,
    sum(`bvsvsm`.`freq_value`) AS `cf`
  FROM (`bible_stemmed_vocabulary` `bsv`
    JOIN `bible_verse_stemmed_vsm` `bvsvsm` ON ((`bsv`.`id` = `bvsvsm`.`word_id`)))
  GROUP BY `bsv`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `v_stemmed_idf_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_idf_scoring`;

CREATE ALGORITHM = TEMPTABLE
  DEFINER =`bible-miner`@`localhost`
  SQL SECURITY DEFINER VIEW
  `v_stemmed_idf_scoring`  AS
  SELECT
    `bsv`.`id`           AS `id`,
    `bsv`.`stemmed_word` AS `stemmed_word`,
    ifnull(log(
               10, (
                 (SELECT count(DISTINCT `bvsvsm1`.`verse_id`)
                  FROM `bible_verse_stemmed_vsm` `bvsvsm1`) /
                 (SELECT count(`bvsvsm2`.`word_id`) AS `df`
                  FROM (`bible_stemmed_vocabulary` `bsv2`
                    JOIN `bible_verse_stemmed_vsm` `bvsvsm2` ON ((`bsv2`.`id` = `bvsvsm2`.`word_id`))
                  )
                  WHERE (`bsv2`.`id` = `bsv`.`id`)))), 0
    )                    AS `idf`
  FROM `bible_stemmed_vocabulary` `bsv`
  ORDER BY `bsv`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `v_stemmed_tfxidf_scoring`
--
DROP TABLE IF EXISTS `v_stemmed_tfxidf_scoring`;

CREATE ALGORITHM = TEMPTABLE
  DEFINER =`bible-miner`@`localhost`
  SQL SECURITY DEFINER VIEW
  `v_stemmed_tfxidf_scoring`  AS
  SELECT
    `brc`.`id`                           AS `verse_id`,
    `brc`.`verse_reference`              AS `ref`,
    `vsis`.`stemmed_word`                AS `word`,
    `bvsv`.`freq_value`                  AS `tf`,
    `vsis`.`idf`                         AS `idf`,
    (`bvsv`.`freq_value` * `vsis`.`idf`) AS `tfxidf`
  FROM (
      (`bible_verse_stemmed_vsm` `bvsv`
        JOIN `bible_raw_content` `brc` ON ((`bvsv`.`verse_id` = `brc`.`id`)))
      JOIN `v_stemmed_idf_scoring` `vsis` ON ((`bvsv`.`word_id` = `vsis`.`id`))
  )
  ORDER BY `brc`.`id`;


SELECT
  brc.id              AS verse_id,
  brc.verse_reference AS ref,
  brc.verse_text      AS verse_text,
  v2.words            AS words,
  bb.canonical_name   AS bibleBookCanonicalName,
  v2.CountOf          AS matches,
  v2.SumOf            AS score
FROM bible_rvgverse_stemmed_vsm bvsv
  INNER JOIN bible_rvgraw_content brc ON brc.id = bvsv.verse_id
  INNER JOIN bible_book bb ON bb.id = brc.book_id
  INNER JOIN (
               SELECT
                 bvsv2.verse_id,
                 GROUP_CONCAT(DISTINCT sv.stemmed_word SEPARATOR ',') AS words,
                 COUNT(*)                                             AS CountOf,
                 SUM(sv.idf_value)                                    AS SumOf
               FROM bible_rvgverse_stemmed_vsm bvsv2
                 INNER JOIN spanish_stemmed_vocabulary sv ON sv.id = bvsv2.word_id
               WHERE sv.stemmed_word IN ('principi', 'cre', 'dios', 'ciel')
               GROUP BY bvsv2.verse_id
               HAVING COUNT(*) > 1
             ) v2 ON v2.verse_id = bvsv.verse_id
GROUP BY brc.id
ORDER BY matches DESC, score DESC

SELECT count(*) AS cnt
FROM (SELECT
        brc.id              AS verse_id,
        brc.verse_reference AS ref,
        brc.verse_text      AS verse_text,
        v2.words            AS words,
        bb.canonical_name   AS bibleBookCanonicalName,
        v2.CountOf          AS matches,
        v2.SumOf            AS score
      FROM bible_rvgverse_stemmed_vsm bvsv INNER JOIN bible_rvgraw_content brc ON brc.id = bvsv.verse_id
        INNER JOIN (SELECT
                      bvsv2.verse_id,
                      GROUP_CONCAT(DISTINCT sv2.stemmed_word SEPARATOR ',') AS words,
                      COUNT(*)                                              AS CountOf,
                      SUM(sv2.idf_value)                                    AS SumOf
                    FROM
                      bible_rvgverse_stemmed_vsm bvsv2 INNER JOIN spanish_stemmed_vocabulary sv ON sv.id = bvsv2.word_id
                    WHERE sv.stemmed_word IN ("dios", "tierr", "ciel", "cre", "principi")
                    GROUP BY bvsv2.verse_id
                    HAVING COUNT(*) > 1) v2 ON v2.verse_id
        = brc.id
        INNER JOIN bible_book bb ON bb.id = brc.book_id
      GROUP BY brc.id
      ORDER BY matches DESC, score DESC) v_counter
